<?php

/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package init
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>

<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<?php wp_body_open(); ?>
	<div id="page" class="site">
		<a class="skip-link screen-reader-text" href="#primary"><?php esc_html_e('Skip to content', 'init'); ?></a>

		<header id="masthead" class="header site-header">
			<div class="container">
				<div class="header__wrapper">
					<div class="header__logo">ZHUMAY KZ</div>
					<?php
					wp_nav_menu([
						'theme_location' => 'menu-1',
						'depth'          => 1,
						'container'      => false,
						'fallback_cb'    => false,
						'echo'           => true,
						'walker'         => new BEM_Walker_Nav_Menu(),
						'bem_block'      => 'nav',
						'items_wrap'     => '
                                        <nav class="header__nav nav">
                                            <ul class="nav__list article-sm">%3$s</ul>
                                        </nav>
                                    ',
					]);
					?>

					<div class="header__icons">
						<a href=""><span class="header__icon icon-instagram"></span></a>
						<a href=""><span class="header__icon icon-whatsapp"></span></a>


					</div>
				</div>
			</div>
		</header><!-- #masthead -->