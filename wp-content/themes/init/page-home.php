<?php

/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package init
 */

get_header();
?>

<main id="primary" class="site-main">

    <?php while (have_posts()) : the_post(); ?>

        <section class="first-slide">
            <div class="container">
                <div class="first-slide__wrapper">
                    <div class="first-slide__label">
                        <div class="first-slide__header article header-block">
                            <div class="header-block__subtitle"><?php _e('Дизайн бюро', 'init') ?></div>
                            <div class="header-block__title"><?php _e('Интерьер вашего дома', 'init') ?></div>
                            <div class="header-block__description"><?php _e('Проектируем интерьер профессионально, знаем все тонкости ремонтно-отделочных работ на практике. В результате ремон, выполненный по дизайну проекта, полностью соответствует эскизам', 'init') ?></div>
                        </div>
                        <div class="first-slide__buttons">
                            <button class="button">Наш проект <span class="button__icon icon-right"></span></button>
                            <button class="button--light">Консультация <span class="button__icon icon-right"></span></button>
                        </div>
                    </div>
                    <div class="first-slide__items">
                        <div class="first-slide__item"><img src="wp-content/themes/init/img/first-slide-1.jpeg" alt=""></div>
                        <div class="first-slide__item"><img src="wp-content/themes/init/img/first-slide-2.jpeg" alt=""></div>
                        <div class="first-slide__item"><img src="wp-content/themes/init/img/first-slide-3.jpg" alt=""></div>
                        <div class="first-slide__item"><img src="wp-content/themes/init/img/first-slide-4.jpeg" alt=""></div>

                    </div>
                </div>
            </div>
        </section>
        <section class="about gm-dark">
            <div class="container">

                <div class="about__wrapper">
                    <div class="about__label">
                        <div class="about__header article header-block">
                            <div class="header-block__subtitle"><?php _e('Факты про бюро', 'init') ?></div>
                            <div class="header-block__title"><?php _e('О нас', 'init') ?></div>
                        </div>
                        <div class="about__content">
                            <div class="about__items">
                                <div class="about__item">
                                    <div class="about__number">5</div>
                                    <div class="about__item">лет опыта работы</div>
                                </div>
                                <div class="about__item">
                                    <div class="about__number">15</div>
                                    <div class="about__item">реализованных проектов с авторским надзором</div>
                                </div>

                            </div>
                            <div class="about__text">
                                <p>Мы проектируем современные лаконичные интерьеры. В их основе всегда лежит проектирование пространства.</p>
                                <p>В наших работах вы не увидите "декоративно-роскошных" интерьеров.</p>
                            </div>
                        </div>
                    </div>
                    <div class="about__photo">
                        <svg class="about__stample" viewBox="0 0 140 140" width="50%">
                            <path d="M70,70m-46,0a46,46 0 1,1 92,0a46,46 0 1,1 -92,0" fill="transparent" id="tophalf" />
                            <text>
                                <textPath xlink:href="#tophalf" startOffset="0%"> zhumay - des</textPath>
                                <textPath xlink:href="#tophalf" startOffset="52%">ign - buro </textPath>
                            </text>
                        </svg>
                        <img src="wp-content/themes/init/img/about.webp" alt="">
                        <div class="about__blur"></div>
                    </div>
                </div>
            </div>
        </section>
    <?php endwhile; ?>

</main><!-- #main -->

<?php
get_footer();
